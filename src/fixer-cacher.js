const Datastore = require("@google-cloud/datastore");
const got = require("got");

/**
 * Кеширователь для запросов к fixer api.
 */
class FixerCacher {
  /**
   * Параметры для FixerCacher
   * @typedef FixerCacherOptions
   * @property {string} apiKey - ключ api
   * @property {number} maxCacheAge - максимальное время жизни кеша в секундах
   */

  /**
   * @typedef FixerResponseSuccess
   * @property {boolean} success
   * @property {number} timestamp UNIX timestamp
   * @property {string} base base currency
   * @property {string} date
   * @property {Object} rates курсы
   */

  /**
   * @typedef FixerResponseError
   * @property {boolean} success
   * @property {Object} error
   * @property {number} error.code
   * @property {string} error.info
   */

  /**
   * @param {FixerCacherOptions} options
   */
  constructor(options) {
    this.apiKey = options.apiKey;
    this.maxCacheAge = options.maxCacheAge;

    this.apiUrl = "http://data.fixer.io/api/";

    this.datastore = new Datastore({
      namespace: "fixer.io"
    });
  }

  /**
   * Get-запрос к fixer api
   * @param {*} endpoint
   * @param {*} query
   * @returns {FixerResponseSuccess | FixerResponseError}
   */
  async fixer_get(endpoint, query = {}) {
    const response = await got.get(endpoint, {
      baseUrl: this.apiUrl,
      query: Object.assign(query, { access_key: this.apiKey })
    });

    return JSON.parse(response.body);
  }

  /**
   * fixer.io/api/symbols
   * @returns {Promise<{success: boolean, symbols: Object}> | FixerResponseError}
   */
  async symbols() {
    // bypass cache
    return this.fixer_get("symbols");
  }

  /**
   * fixer.io/api/latest
   * Для простоты кеширования оно не принимает аргументы, описанные в fixer.io api
   */
  async latest() {
    const key = this.datastore.key(["temporary", "latest"]);

    try {
      const entities = await this.datastore.get(key);

      // check if entity exists
      if (entities.length > 0) {
        // has to be a single entity
        const entity = entities[0];

        // check timestamp and decide
        if (entity.timestamp !== undefined) {
          const now = Date.now() / 1000;
          if (now - entity.timestamp < this.maxCacheAge) {
            // still valid
            return entity;
          }
        }
      }

      // either create a new entity or refresh an old one
      const data = await this.fixer_get("latest");
      this.datastore
        .save({
          key: key,
          data: data
        })
        .catch(err => {
          console.error("Error creating/updating entity", key.path, err);
        });

      return data;

    } catch (err) {
      console.error(err);
      return {
        success: false,
        error: {
          code: -1,
          info: "Error getting latest data from GCP Datastore"
        }
      };
    }
  }

  /**
   * fixer.io/api/{year}-{month}-{day}
   * @param {Object} date
   * @param {number} date.year
   * @param {number} date.month
   * @param {number} date.day
   */
  async historical(date) {
    const { year, month, day } = date;
    const dateStr = `${year}-${month}-${day}`;
    const key = this.datastore.key(["permanent", "historical", dateStr]);

    // TODO cache hit
    // return this.fixer_get(dateStr);

    return {
      success: false,
      error: {
        code: -1,
        info: "NOT IMPLEMENTED YET"
      }
    };
  }
}

exports.FixerCacher = FixerCacher;
