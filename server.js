// загружаем переменные окружения из .env
require("dotenv").config();

// загружаем конфиг
const config = require("./config");

const express = require("express");
const cors = require("cors");

const app = express();
app.use(cors());

const { FixerCacher } = require("./src/fixer-cacher");

const fixerCacher = new FixerCacher({
  apiKey: config["fixer.io"].apiKey,
  maxCacheAge: 60 * 60 * 24
});

app.get("/fixer/symbols", async (req, res) => {
  const result = await fixerCacher.symbols();
  res
    .status(200)
    .send(result)
    .end();
});

app.get("/fixer/latest", async (req, res) => {
  const result = await fixerCacher.latest();

  res
    .status(200)
    .send(result)
    .end();
});

app.get("/fixer/historical/:year/:month/:day", async (req, res) => {
  const { year, month, day } = req.params;

  const result = await fixerCacher.historical({
    year,
    month,
    day
  });

  res
    .status(200)
    .send(result)
    .end();
});

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log("Press Ctrl+C to quit.");
});
