# currency-web-service

Сервис для кеширования обменных курсов валют из источников с ограниченной квотой. Для Google Cloud Platform.

# Требования к окружению

На компьютере должны быть установлены:
- Google Cloud SDK ([инструкция](https://cloud.google.com/appengine/docs/standard/nodejs/quickstart))
- node, npm

# Кастомизация

Для работы сервиса необходим api ключ для бесплатного сервиса [fixer.io](https://fixer.io/).

Ключ помещается в `config.js`.

# Разработка на локальной машине

Чтобы протестировать сервис локально, необходимо получить сервисный ключ в консоли проекта Google Cloud.

Путь к этому ключу добавляется в файл `.env` (см. `.env.example`).

Этот ключ предназначен для использования на локальной машине. Файл `.env` не следует загружать в систему контроля версий. При загрузке проекта в Google Cloud Platform переменная окружения `GOOGLE_APPLICATION_CREDENTIALS` будет автоматически создана Гуглом.

Установка зависимостей:
```
npm i
```

# Загрузка в Google Cloud Platform

Необходимо установить Cloud SDK, создать проект и инициализировать AppEngine ([инструкция](https://cloud.google.com/appengine/docs/standard/nodejs/quickstart)).

Главным образом задействуются эти команды:

- [`gcloud projects create`](https://cloud.google.com/sdk/gcloud/reference/projects/create)
- [`gcloud app create`](https://cloud.google.com/sdk/gcloud/reference/app/create)
- [`gcloud app deploy`](https://cloud.google.com/sdk/gcloud/reference/app/deploy)

